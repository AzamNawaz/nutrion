import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  newCategoryTitle: string = '';
  addNewCat: boolean = false;
  message: string = '';
  addStatus: boolean = false;
  addingNewCat: boolean = false;
  categories: any = [];
  catTitleToEdit: string = '';
  filteredCategory: any;
  editingCategory: boolean = false;
  constructor(private http: HttpClient, private apiService: ApiService) { }

  ngOnInit(): void {
    this.getCategories();
  }
  getCategories() {
    const httpHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token')
    })
    this.apiService.getRequest('admin/getCategories', httpHeaders).subscribe(res => {
      console.log(res);
      if (res.info.status == 200) {
        this.categories = res.categories;
      }
    })
  }
  deleteCategory(category: any) {
    console.log(category);
    const httpHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token')
    })
    this.apiService.getRequest(`admin/deleteCategory/${category._id}`, httpHeaders).subscribe(res => {
      console.log(res);
      if (res.info.status == 200) {
        this.getCategories();
      }
    })
  }
  showOrHideCategory(category: any) {
    category.isActive = !category.isActive
    const httpHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token'),
      'Content-Type': 'application/json'
    })
    this.apiService.getRequest(`admin/ActivateOrDeactivateCategory/${category._id}/${category.isActive}`, httpHeaders).subscribe(res => {
      console.log(res);
      if (res.info.status == 200) {
        this.getCategories();
      } else {
        category.isActive = !category.isActive
      }
    },error=>{
      category.isActive = !category.isActive
    })
  }
  editCategory(category: any) {
    this.editingCategory = true;
    this.filteredCategory = category;
    this.catTitleToEdit = category.title;
  }
  updateCategoryTitle() {
    const httpHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token')
    })
    this.apiService.getRequest(`admin/updateCategoryTitle/${this.filteredCategory._id}/${this.catTitleToEdit}`, httpHeaders).subscribe(res => {
      console.log(res);
      if (res.info.status == 200) {
        this.editingCategory = false;
        this.getCategories();
      }
    })
  }
  addCategory() {
    this.addingNewCat = true;
    let body = {
      title: this.newCategoryTitle
    }
    const httpsHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token')
    })
    this.apiService.postRequest('admin/addNewCategory', body, httpsHeaders).subscribe(res => {
      console.log(res);
      this.addingNewCat = false;
      this.addStatus = res.info.status == 200 ? true : false;
      this.message = res.info.message;
      if (this.addStatus) {
        this.getCategories();
      }
    })
  }
}
