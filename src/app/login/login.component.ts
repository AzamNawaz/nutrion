import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { loginService } from './login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  constructor(private router: Router, private http: HttpClient, private loginService: loginService, private app: AppComponent) {
    this.app.setHeaderVisibility(false);
    this.app.setSideNavVisibility(false);
  }

  ngOnInit(): void {
  }
  ngOnDestroy() {
    this.app.setHeaderVisibility(true);
    this.app.setSideNavVisibility(true);
  }
  submit(form: NgForm) {
    console.log(form.controls.email.value);
    const email = form.controls.email.value;
    const password = form.controls.password.value;
    this.loginService.login(email, password).subscribe(res => {
      console.log(res.info);
      if (res.info.status == 200) {
        localStorage.setItem('token', res.token);
        this.router.navigateByUrl('/home');
      }
    });;
    /* const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    })
    const body = {
      email: form.controls.email.value,
      password: form.controls.password.value
    }
    this.http.post<any>('http://localhost:8080/admin/login', JSON.stringify(body), { headers: httpHeaders }).subscribe(res => {
      console.log(res.info);
      if (res.info.status == 200) {
        localStorage.setItem('token', res.token);
        this.router.navigateByUrl('/home');
      }
    }) */
  }
}
