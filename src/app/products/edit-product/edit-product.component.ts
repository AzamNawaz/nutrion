import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  @Input() product: any;
  @Input() editing: boolean = false;
  @Output() closeModal = new EventEmitter<boolean>();
  title: string = '';
  description: string = '';
  stock: boolean = false;
  isActive: boolean = false;
  price: number = 1;
  @Input() categories: any = [];
  categoryName: string = '';
  selectedCategory: any;
  selectedCategoryId: any;
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    if (this.editing) {
      console.log(this.product);
      this.title = this.product?.title;
      this.description = this.product?.description;
      this.stock = this.product?.stock ? this.product?.stock : false;
      this.isActive = this.product?.isActive ? this.product?.isActive : false;
      this.price = this.product.price;
      this.selectedCategory = this.product.category;
      this.selectedCategoryId = this.selectedCategory._id;
      this.categoryName = this.selectedCategory.title;
    } else {
      this.selectedCategory = this.categories[0];
      this.selectedCategoryId = this.selectedCategory._id;
      this.categoryName = this.selectedCategory.title;
    }
  }
  submitForm(form: NgForm) {
    console.log(form.controls);
    if (!this.editing) {
      this.addNewProduct();
    }else{
      this.updateProduct();
    }
  }
  addNewProduct() {
    const httpHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token'),
      'Content-Type': 'application/json'
    })
    const body = {
      title: this.title,
      description: this.description,
      stock: this.stock,
      isActive: this.isActive,
      category: this.selectedCategoryId,
      price: this.price
    }
    this.apiService.postRequest('admin/addNewProduct', body, httpHeaders).subscribe(res => {
      console.log(res);
      if (res.info.status == 200) {
        this.closeModal.emit(true);
      }
    })
  }
  updateProduct(){
    const httpHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token'),
      'Content-Type': 'application/json'
    })
    const body = {
      title: this.title,
      description: this.description,
      stock: this.stock,
      isActive: this.isActive,
      category: this.selectedCategoryId,
      price: this.price
    }
    this.apiService.postRequest(`admin/updateProduct/${this.product._id}`, body, httpHeaders).subscribe(res => {
      console.log(res);
      if (res.info.status == 200) {
        this.closeModal.emit(true);
      }
    })
  }
  close() {
    this.closeModal.emit(false);
  }
  categoryChanged() {
    this.categories.forEach((element: any) => {
      if(element.title == this.categoryName){
        this.selectedCategory = element
      };
    });
    console.log(this.selectedCategory);
    this.selectedCategoryId = this.selectedCategory._id
  }
}
