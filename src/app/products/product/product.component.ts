import { Component, Input, OnInit, Output ,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() product:any;
  @Output() edit = new EventEmitter<any>()
  constructor() { }

  ngOnInit(): void {
  }
  editProduct(){
    this.edit.emit(this.product);
  }
}
