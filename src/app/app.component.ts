import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isHeaderVisible: boolean = true;
  isSideNavVisible: boolean = true;
  title = 'nutrion';
  setHeaderVisibility(visibilty: boolean) {
    this.isHeaderVisible = visibilty
  }
  setSideNavVisibility(visibilty: boolean) {
    this.isSideNavVisible = visibilty
  }
}
