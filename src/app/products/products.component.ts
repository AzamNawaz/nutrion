import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  addNewProduct: boolean = false;
  filteredProduct: any;
  categories: any;
  categoryName: string = '';
  products: any = [];
  selectedCategory: undefined;
  editing: boolean = false;
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.getCategories();
  }
  getCategories() {
    const httpHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token')
    })
    this.apiService.getRequest('admin/getCategories', httpHeaders).subscribe(res => {
      console.log(res);
      if (res.info.status == 200) {
        this.categories = res.categories;
        this.categoryName = this.categories[0].title;
        this.selectedCategory = this.categories[0]
        this.getProducts(this.selectedCategory);
      }
    })
  }
  getProducts(category: any) {
    const httpHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token')
    })
    this.apiService.getRequest(`admin/getProducts/${category._id}`, httpHeaders).subscribe(res => {
      console.log(res);
      if (res.info.status == 200) {
        this.products = res.products;
      }
    })
  }
  addNewProducts() {
    this.editing = false;
    this.filteredProduct = null;
    this.addNewProduct = true
  }
  categoryChanged() {
    let category;
    this.categories.forEach((element: any) => {
      if (element.title == this.categoryName) {
        category = element
      };
    });
    this.selectedCategory = category;
    this.getProducts(category);
  }
  closeModal(event: any) {
    this.addNewProduct = false;
    if (event) {
      this.getProducts(this.selectedCategory);
    }
  }
  editProduct(product: any) {
    this.editing = true;
    this.filteredProduct = product;
    this.addNewProduct = true;
  }
}
