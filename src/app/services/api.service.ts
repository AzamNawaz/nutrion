import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment"
@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(private http: HttpClient) { }
    postRequest(url: string, params: Object, headers?: HttpHeaders) {
        return this.http.post<any>(this.setUrl(url), params, { headers: headers })
    }
    getRequest(url: string, headers?: HttpHeaders): Observable<any> {
        return this.http.get<any>(this.setUrl(url), { headers: headers })
    }
    setUrl(url: string) {
        return environment.apiEndPoint + url;
    }
}