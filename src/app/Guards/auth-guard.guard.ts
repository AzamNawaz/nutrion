import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { ApiService } from '../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(private router: Router, private apiService: ApiService) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = !!localStorage.getItem('token')
    const httpHeaders = new HttpHeaders({
      'Authorization': 'bearer ' + localStorage.getItem('token')
    })
    let auth = new Subject<boolean | UrlTree>();
    if (!token) {
      this.router.navigateByUrl('/login')
      return false;
    }
    this.apiService.getRequest('admin/verifyToken', httpHeaders).subscribe(res => {
      console.log(res);
      if (res.info.status == 200) {
        auth.next(true);
      } else {
        localStorage.removeItem('token');
        this.router.navigateByUrl('/login')
        auth.next(false);
      }
    })

    return auth;
  }

}
