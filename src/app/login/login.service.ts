import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class loginService {
    private isLoggedInSub = new BehaviorSubject<boolean>(false);
    isLoggedIn: Observable<boolean> = this.isLoggedInSub.asObservable();
    constructor(private http: HttpClient) { }

    login(email: string, pass: string): Observable<any> {
        const httpHeaders = new HttpHeaders({
            'Content-Type': 'application/json'
        })
        const body = {
            email: email,
            password: pass
        }
        return this.http.post<any>('http://localhost:8080/admin/login', JSON.stringify(body), { headers: httpHeaders })
    }
    
}